; defines C like for
(define-syntax for
   (syntax-rules (.. in)
      ((for var in low .. sup code ...) (for var low (< var sup) (+ var 1) code ...))
      ((for var in coll code ...)
                 (cond
                    ((list? coll)
                        (for-each (lambda (var) code ...) coll))
                    ((vector? coll)
                        (vector-for-each (lambda (var) code ...) coll))
                    ((string? coll)
                        (string-for-each (lambda (var) code ...) coll))
                    (else (error "unsupported for" coll))))
      ((for var low test inc code ...)  (do ((var low inc)) ((not test)) code ...))))

(for i in 0 .. 10
	(display i)
	(newline))

(for i in "abcd"
	(display i)
	(newline))

(for i in '(a z e r)
	(display i)
	(newline))

(for i in #(34 56 78)
	(display i)
	(newline))

(for i in 7
	(display i)
	(newline))

